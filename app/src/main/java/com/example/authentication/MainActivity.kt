package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        backToMain()
    }
    private fun backToMain(){
        back.setOnClickListener {
            val intent = Intent(this,AuthenticationActivity::class.java)
            startActivity(intent)
        }
    }

}